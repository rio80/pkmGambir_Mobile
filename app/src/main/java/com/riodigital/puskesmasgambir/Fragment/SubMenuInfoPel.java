package com.riodigital.puskesmasgambir.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuAlurPelayanan;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuFasKes;
import com.riodigital.puskesmasgambir.adapter.InfoPelGridAdapter;

/**
 * Created by rio senjou on 25/08/2017.
 */

public class SubMenuInfoPel extends Fragment {
    String[] nama = {
            "Fasilitas Kesehatan",
            "Info Alur Pelayanan"
    };
    int[] gambar = {
            R.drawable.ikesehatan,
            R.drawable.ipelayanan
    };

    GridView grid;

    public SubMenuInfoPel() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_dash_board, container, false);

        grid=rootView.findViewById(R.id.gridview);

        InfoPelGridAdapter infoPelGridAdapter=new InfoPelGridAdapter(getActivity(),nama,gambar);
        grid.setAdapter(infoPelGridAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=null;
                if(i==0){
                    intent = new Intent(getActivity(), SubMenuFasKes.class);
                }else if(i==1){
                    intent = new Intent(getActivity(), SubMenuAlurPelayanan.class);
                }
                startActivity(intent);
            }
        });
    }
}
