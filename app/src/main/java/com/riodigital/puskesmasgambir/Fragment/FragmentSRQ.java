package com.riodigital.puskesmasgambir.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.SubMenu.RegisterSRQ;
import com.riodigital.puskesmasgambir.SubMenu.SRQ;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSRQ extends Fragment {

    Button mulaiSrq;
    public FragmentSRQ() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_srq, container, false);
        mulaiSrq=(Button)view.findViewById(R.id.mulaiSrq);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mulaiSrq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),RegisterSRQ.class));
            }
        });
    }

}
