package com.riodigital.puskesmasgambir.Fragment;


import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuDasarHukum;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuDeskripsiApp;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuVisiMisi;
import com.riodigital.puskesmasgambir.adapter.PksGbrGridAdapter;

public class SubMenuPksGbr extends Fragment {

    public SubMenuPksGbr() {

    }

    String[] nama={
            "Visi Misi",
            "Deskripsi Aplikasi",
            "Dasar Hukum"
    };
    int[] gambar={
            R.drawable.iguard,
            R.drawable.ideskripsi,
            R.drawable.index
    };
    GridView grid;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.activity_dash_board,container,false);

        grid= rootView.findViewById(R.id.gridview);

        PksGbrGridAdapter pksGbrGridAdapter=new PksGbrGridAdapter(getActivity(),nama,gambar);
        grid.setAdapter(pksGbrGridAdapter);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=null;
               if(i==0){
                   intent = new Intent(getActivity(), SubMenuVisiMisi.class);
               }else if(i==1){
                   intent = new Intent(getActivity(), SubMenuDeskripsiApp.class);
               }else if(i==2){
                   intent = new Intent(getActivity(), SubMenuDasarHukum.class);
               }
                startActivity(intent);
            }
        });
    }


}
