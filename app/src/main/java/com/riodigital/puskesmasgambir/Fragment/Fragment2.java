package com.riodigital.puskesmasgambir.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.adapter.ProgrammingAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {
    ListView listProgram;
    private String[] nama_program={
            "Java",
            "JAVASCRIPT",
            "PHP",
            "MYSQL"
    };
    private String[] keterangan={
            "Java is the best",
            "more speed",
            "More Easy to learn",
            "More speed data access"
    };
    private Integer[] gambar={
            R.drawable.java,
            R.drawable.cplusplus,
            R.drawable.aspnet,
            R.drawable.pythonlogo
    };

    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment2,container,false);

        ProgrammingAdapter programmingAdapter=new ProgrammingAdapter
                (getActivity(),container,nama_program,keterangan,gambar);

        listProgram=rootView.findViewById(R.id.list_program);
        listProgram.setAdapter(programmingAdapter);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listProgram=(ListView)view.findViewById(R.id.list_program);
        listProgram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String hasil;
                hasil=nama_program[i];
                Toast.makeText(getActivity(),"Anda memilih : "+ hasil,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
