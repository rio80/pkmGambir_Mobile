package com.riodigital.puskesmasgambir.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.SubMenu.SubMenuVisiMisi;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {
    Button btn_VisiMisi;

    public Fragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment3, container, false);
        return viewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_VisiMisi=(Button)view.findViewById(R.id.tombol1);
        btn_VisiMisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity().getApplicationContext(), SubMenuVisiMisi.class);
                startActivity(i);
            }
        });
    }
}
