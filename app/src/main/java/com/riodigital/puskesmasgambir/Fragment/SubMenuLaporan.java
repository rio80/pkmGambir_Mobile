package com.riodigital.puskesmasgambir.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.SubMenu.Laporan;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubMenuLaporan extends Fragment {
    TextView tampil_contentLaporan;
    Button btn_laporan;

    public SubMenuLaporan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_menu_laporan, container, false);

        tampil_contentLaporan = view.findViewById(R.id.content_laporan);
        tampil_contentLaporan.setText(Html.fromHtml(getString(R.string.laporan)));
        btn_laporan=view.findViewById(R.id.button_Laporan);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_laporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(),Laporan.class);
                startActivity(i);
            }
        });
    }


}
