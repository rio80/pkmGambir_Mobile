package com.riodigital.puskesmasgambir.DashBoard;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.riodigital.puskesmasgambir.MainActivity;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.adapter.DashboardGridAdapter;

public class DashBoard extends AppCompatActivity {
    Button btnPuskesmasGambir, btnInfoPelayanan, btnInfoKes, btnLaporan, btnDeteksiDini;


    public void init() {
        btnPuskesmasGambir = (Button) findViewById(R.id.btnPuskesmasGambir);
        btnInfoPelayanan = (Button) findViewById(R.id.btnInfoPelayanan);
        btnInfoKes = (Button) findViewById(R.id.btnInfoKes);
        btnLaporan = (Button) findViewById(R.id.btnLaporan);
        btnDeteksiDini = (Button) findViewById(R.id.btnDeteksiDini);
    }
    GridView gridview;
    String[] values = {
            "Puskesmas Gambir",
            "Info Pelayanan",
            "Info Kesehatan",
            "Laporan",
            "Deteksi Dini",
            "EXIT"
    };
    int[] gambar = {
            R.drawable.iredcross,
            R.drawable.ipelayanan,
            R.drawable.ikesehatan,
            R.drawable.ilaporan,
            R.drawable.ideteksidini,
            R.drawable.iexit,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        init();
        gridview=(GridView)findViewById(R.id.gridview);

        DashboardGridAdapter gridAdapter=new DashboardGridAdapter(this,values,gambar);
        gridview.setAdapter(gridAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("indexMenu",i);
                if(i==5) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashBoard.this);

                    // set title dialog
                    alertDialogBuilder.setTitle("Keluar dari aplikasi?");

                    // set pesan dari dialog
                    alertDialogBuilder
                            .setMessage("Klik Ya untuk keluar!")
                            .setIcon(R.mipmap.ic_launcher)
                            .setCancelable(false)
                            .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // jika tombol diklik, maka akan menutup activity ini
                                    DashBoard.this.finish();
                                    System.exit(0);
                                }
                            })
                            .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // jika tombol ini diklik, akan menutup dialog
                                    // dan tidak terjadi apa2
                                    dialog.cancel();
                                }
                            });

                    // membuat alert dialog dari builder
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // menampilkan alert dialog
                    alertDialog.show();
                }else{
                    startActivity(intent);
                }

            }
        });


    }



}
