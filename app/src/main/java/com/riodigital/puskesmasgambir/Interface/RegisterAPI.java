package com.riodigital.puskesmasgambir.Interface;

import com.riodigital.puskesmasgambir.model.Value;
import com.riodigital.puskesmasgambir.model.reportSrq;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by rio senjou on 30/08/2017.
 */

public interface RegisterAPI {
    @FormUrlEncoded
    @POST("/insert.php")
    Call<Value> simpani(@Field("nama") String nama,
                        @Field("alamat") String alamat,
                        @Field("keluhan") String keluhan,
                        @Field("kelamin") String kelamin);

    @GET("srqBinding.php")
    Call<Value> srqBinding();

    @FormUrlEncoded
    @POST("insert_srq.php")
    Call<Value> insertSrqMaster(@Field("id_response") int id_response,
                                @Field("alamat") String alamat,
                                @Field("telepon") String telepon,
                                @Field("email") String email,
                                @Field("user") String user);

    @FormUrlEncoded
    @POST("insert_srq_detail.php")
    Call<Value> insertSrqDetail(@Field("id_response") int id_response,
                                @Field("id_pertanyaan") int id_pertanyaan,
                                @Field("jawaban") int jawaban,
                                @Field("user") String user);

    @GET("srq_report.php")
    Call<reportSrq> viewReportSrq(@Query("idUser") int idUser);

    @GET("cekIdResponse.php")
    Call<Value> max_id();

    @GET("cekUserRegister.php")
    Call<Value> cekUser(@Query("email") String email,
                        @Query("user") String user);

    @GET("cekTanggal.php")
    Call<Value> cekTanggal(@Query("email") String email);
}
