package com.riodigital.puskesmasgambir.model;

/**
 * Created by rio senjou on 21/12/2017.
 */

public class ResponseDetail {
    String idResponse;
    String idPertanyaan;
    int jawaban;
    String user;

    public ResponseDetail(String idResponse, String idPertanyaan, int jawaban, String user) {
        this.idResponse = idResponse;
        this.idPertanyaan = idPertanyaan;
        this.jawaban = jawaban;
        this.user = user;
    }

    public String getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(String idResponse) {
        this.idResponse = idResponse;
    }

    public String getIdPertanyaan() {
        return idPertanyaan;
    }

    public void setIdPertanyaan(String idPertanyaan) {
        this.idPertanyaan = idPertanyaan;
    }

    public int getJawaban() {
        return jawaban;
    }

    public void setJawaban(int jawaban) {
        this.jawaban = jawaban;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
