package com.riodigital.puskesmasgambir.model;

/**
 * Created by rio senjou on 21/12/2017.
 */

public class ResponseMaster {
    String idResponse;
    String alamat;
    String telpon;
    String email;
    String user;
    String tanggal;

    public ResponseMaster(String idResponse, String alamat, String telpon, String email, String user) {
        this.idResponse = idResponse;
        this.alamat = alamat;
        this.telpon = telpon;
        this.email = email;
        this.user = user;
    }

    public String getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(String idResponse) {
        this.idResponse = idResponse;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
