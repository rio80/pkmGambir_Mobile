package com.riodigital.puskesmasgambir.model;

import java.util.List;

/**
 * Created by rio senjou on 30/08/2017.
 */

public class Value {

    String value;
    String message;
    String tglDb;
    String tglSrv;
    int max_id;
    List<bindSrq> result;

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public List<bindSrq> getResult() {
        return result;
    }

    public void setResult(List<bindSrq> result) {
        this.result = result;
    }

    public int getMax_id() {
        return max_id;
    }

    public String getTglDb() {
        return tglDb;
    }

    public String getTglSrv() {
        return tglSrv;
    }
}
