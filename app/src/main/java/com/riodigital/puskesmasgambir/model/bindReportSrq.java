package com.riodigital.puskesmasgambir.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rio senjou on 22/12/2017.
 */

public class bindReportSrq {

    int jmlSrq;
    String nomorSrq;
    String indikasi;

    public bindReportSrq(int jmlSrq, String nomorSrq, String indikasi) {
        this.jmlSrq = jmlSrq;
        this.nomorSrq = nomorSrq;
        this.indikasi = indikasi;
    }

    public int getJmlSrq() {
        return jmlSrq;
    }

    public String getNomorSrq() {
        return nomorSrq;
    }

    public String getIndikasi() {
        return indikasi;
    }
}
