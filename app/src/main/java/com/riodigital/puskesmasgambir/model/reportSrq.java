package com.riodigital.puskesmasgambir.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rio senjou on 22/12/2017.
 */

public class reportSrq {
    @SerializedName("user")
    String user;
    @SerializedName("email")
    String email;
    @SerializedName("result")
    List<bindReportSrq> result;

    public reportSrq(String user, String email, List<bindReportSrq> result) {
        this.user = user;
        this.email = email;
        this.result = result;
    }

    public String getUser() {
        return user;
    }

    public String getEmail() {
        return email;
    }

    public List<bindReportSrq> getResult() {
        return result;
    }
}
