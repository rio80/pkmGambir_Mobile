package com.riodigital.puskesmasgambir.model;

/**
 * Created by rio senjou on 21/12/2017.
 */

public class bindSrq {
    int id;
    String pertanyaan;
    int jawab;

    public bindSrq(int id, String pertanyaan, int jawab) {
        this.id = id;
        this.pertanyaan = pertanyaan;
        this.jawab = jawab;
    }

    public bindSrq(int id, String pertanyaan) {
        this.id = id;
        this.pertanyaan = pertanyaan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public int getJawab() {
        return jawab;
    }

    public void setJawab(int jawab) {
        this.jawab = jawab;
    }
}
