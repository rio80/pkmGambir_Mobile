package com.riodigital.puskesmasgambir;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.riodigital.puskesmasgambir.Fragment.FragmentSRQ;
import com.riodigital.puskesmasgambir.Fragment.SubMenuInfoPel;
import com.riodigital.puskesmasgambir.Fragment.SubMenuLaporan;
import com.riodigital.puskesmasgambir.Fragment.SubMenuPksGbr;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.adapter.SlidingMenuAdapter;
import com.riodigital.puskesmasgambir.model.ItemSlideMenu;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<ItemSlideMenu> listSliding;
    private SlidingMenuAdapter adapter;
    private ListView listViewSliding;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    public int setMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init Component
        listViewSliding = (ListView) findViewById(R.id.lv_sliding_menu);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listSliding = new ArrayList<>();
        //Add item for sliding List
        listSliding.add(new ItemSlideMenu(R.drawable.iredcross, "PUSKESMAS GAMBIR"));
        listSliding.add(new ItemSlideMenu(R.drawable.ipelayanan, "Info Pelayanan"));
        listSliding.add(new ItemSlideMenu(R.drawable.ikesehatan, "Info Kesehatan"));
        listSliding.add(new ItemSlideMenu(R.drawable.ilaporan, "Laporan"));
        listSliding.add(new ItemSlideMenu(R.drawable.ideteksidini, "Deteksi Dini"));
        listSliding.add(new ItemSlideMenu(R.drawable.index, "Dashboard"));
        adapter = new SlidingMenuAdapter(this, listSliding);
        listViewSliding.setAdapter(adapter);


        //Display icon to open/close Sliding list

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(listSliding.get(0).getTitle());
        //Item selected
        listViewSliding.setItemChecked(0, true);
        //Close Menu
        drawerLayout.closeDrawer(listViewSliding);
        //Display Fragment 1 when start
        Intent i = getIntent();
        setMenu = i.getIntExtra("indexMenu", 0);

        replaceFragment(setMenu);


        //Handle on item Click
        listViewSliding.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //set Title
                setTitle(listSliding.get(i).getTitle());
                //Item Selected
                listViewSliding.setItemChecked(i, true);
                //Replace Fragment
                replaceFragment(i);
                if(i==5){
                   MainActivity.this.finish();

                }
                //Close Menu
                drawerLayout.closeDrawer(listViewSliding);
            }
        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //for opened menu list if line left side click
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        //show fragment from item menu selected
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    //Created method replace fragment
    private void replaceFragment(int pos) {
        Fragment fragment=null;

        switch (pos) {
            case 0:
                fragment = new SubMenuPksGbr();
                break;
            case 1:
                fragment = new SubMenuInfoPel();
                break;
            case 2:
                fragment = new SubMenuPksGbr();
                break;
            case 3:
                fragment = new SubMenuLaporan();
                break;
            case 4:
                fragment = new FragmentSRQ();
                break;

            default:
                fragment = new SubMenuPksGbr();
                break;
        }
        if (null != fragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .addToBackStack("tag").commit();
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
