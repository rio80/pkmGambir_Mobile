package com.riodigital.puskesmasgambir.SubMenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.riodigital.puskesmasgambir.Fragment.SubMenuInfoPel;
import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 26/08/2017.
 */

public class JumpInfoPel extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        /*Intent i;
        i=new Intent(this, SubMenuInfoPel.class);
        startActivity(i);*/

        Fragment fragment;
        fragment = new SubMenuInfoPel();
        if (null != fragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .addToBackStack(null).commit();
        }

    }
}
