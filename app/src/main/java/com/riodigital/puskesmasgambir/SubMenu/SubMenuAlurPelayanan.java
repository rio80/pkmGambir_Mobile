package com.riodigital.puskesmasgambir.SubMenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.webkit.WebView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

public class SubMenuAlurPelayanan extends AppCompatActivity {
    WebView webView;
    String URL= "file:///android_asset/alur_pelayanan.htm";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu_alur_pelayanan);
       /* alurPelayanan=(TextView) findViewById(R.id.info_pel);
        alurPelayanan.setText(Html.fromHtml(getString(R.string.info_pel)));*/

       webView=(WebView)findViewById(R.id.info_pel);
        webView.loadUrl(URL);

    }
}
