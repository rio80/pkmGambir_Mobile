package com.riodigital.puskesmasgambir.SubMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.riodigital.puskesmasgambir.Interface.RegisterAPI;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.model.Value;

import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

import com.android.volley.Response;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Laporan extends AppCompatActivity {
    String pilihanSpinner;
    Spinner spinner;
    public static TelephonyManager tel;
    public static final String insertURL = "http://gkm.puskesmaskecgambir.web.id/transaksi/insert.php";
    RequestQueue requestQueue;
    private ProgressDialog progress;

    @BindView(R.id.RPria)
    RadioButton RPria;
    @BindView(R.id.RWanita)
    RadioButton RWanita;
    @BindView(R.id.edit_Nama)
    EditText editNama;
    @BindView(R.id.edit_Alamat)
    EditText editAlamat;
    @BindView(R.id.edit_isiLaporan)
    EditText editIsiLaporan;
    @BindView(R.id.edit_NoTelp)
    EditText editNoTelp;
    @BindView(R.id.edit_noKtp)
    EditText editNoKtp;
    @BindView(R.id.edit_kec)
    EditText editKec;

    @OnClick(R.id.button_simpan)
    void simpan() {
        //membuat progress dialog
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.show();

        //mengambil data dari edittext
        final String nama = editNama.getText().toString();
        final String alamat = editAlamat.getText().toString();
        final String keluhan = editIsiLaporan.getText().toString();
        final String notelp = editNoTelp.getText().toString();
        final String noktp = editNoKtp.getText().toString();
        final String kec = editKec.getText().toString();

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest request = new StringRequest(Request.Method.POST,
                insertURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),
                                "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                error.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                String kelamin = null;
                parameters.put("nama", nama);
                parameters.put("alamat", alamat);
                parameters.put("keluhan", keluhan);
                parameters.put("notelp", notelp);
                parameters.put("NoKtp", noktp);
                parameters.put("kecamatan", kec);
                parameters.put("kelurahan", pilihanSpinner);

                if (RPria.isChecked()) {
                    kelamin = RPria.getText().toString();
                } else if (RWanita.isChecked()) {
                    kelamin = RWanita.getText().toString();
                }
                parameters.put("kelamin", kelamin);
                return parameters;
            }
        };
        requestQueue.add(request);
        progress.dismiss();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);//Untuk membuat event dan binding data


        /*spinner=(EditText)findViewById(R.id.spinner_wilayah);
        spinner.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                kelurahan
        ));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

      /*  spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] kelurahan = {
                        "Gambir",
                        "Petojo Utara",
                        "Petojo Selatan",
                        "Duri Puloh",
                        "Kebon Kelapa",
                        "Cideng"
                };
                AlertDialog.Builder builder=new AlertDialog.Builder(getParent());
                builder.setTitle("Pilih Kelurahan");
                builder.setItems(kelurahan, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"Wilayah Terpilih : "+kelurahan[i],Toast.LENGTH_SHORT).show();
                        pilihanSpinner=kelurahan[i].toString();
                    }
                }).show();
            }
        });*/
        spinner = (Spinner) findViewById(R.id.spinner_wilayah);
        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(spinner.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner.getSelectedItem()))) {
                // ToDo when first item is selected
            } else {
                Toast.makeText(parent.getContext(),
                        "Kamu memilih : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
                pilihanSpinner = parent.getItemAtPosition(pos).toString();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }
}
