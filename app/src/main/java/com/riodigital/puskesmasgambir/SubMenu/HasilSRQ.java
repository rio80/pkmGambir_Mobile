package com.riodigital.puskesmasgambir.SubMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.riodigital.puskesmasgambir.ApiClient;
import com.riodigital.puskesmasgambir.Interface.RegisterAPI;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.model.bindReportSrq;
import com.riodigital.puskesmasgambir.model.reportSrq;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HasilSRQ extends AppCompatActivity {
    SharedPreferences pref;
    SharedPreferences prefReg;
    SharedPreferences.Editor editor;
    RegisterAPI regApi;
    private ProgressDialog progress;
    Button kembali;
    List<bindReportSrq> listReport = new ArrayList<>();
    private RegisterSRQ regSrq;
    private SRQ srq;
    TextView hasilUser, hasilAlamat, hasilEmail, hasilIndikasi;
    int id_response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_srq);

        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Tunggu Sebentar...");
        progress.show();

        hasilUser = (TextView) findViewById(R.id.hasilUser);
        hasilAlamat = (TextView) findViewById(R.id.hasilAlamat);
        hasilEmail = (TextView) findViewById(R.id.hasilEmail);
        hasilIndikasi = (TextView) findViewById(R.id.indikasi);
        kembali=(Button)findViewById(R.id.hsl_kembali);


        try {
            pref = getSharedPreferences(regSrq.KEY_PREF, Context.MODE_PRIVATE);

            hasilUser.setText("Nama : " + pref.getString(regSrq.KEY_USER, ""));
            hasilAlamat.setText("Alamat : " + pref.getString(regSrq.KEY_ALAMAT, ""));
            hasilEmail.setText("Email : " + pref.getString(regSrq.KEY_EMAIL, ""));

            Intent intent = getIntent();
            id_response = intent.getIntExtra("id_response", 0);
            hasilIndikasi.setText("");
            regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);
            Call<reportSrq> callReportSrq = regApi.viewReportSrq(id_response);
            callReportSrq.enqueue(new Callback<reportSrq>() {
                @Override
                public void onResponse(Call<reportSrq> call, Response<reportSrq> response) {
                    listReport = response.body().getResult();
                    progress.dismiss();
                    for (bindReportSrq report : listReport) {
                        hasilIndikasi.append("Anda Terindikasi : " + report.getIndikasi() + "\n");
                    }

                    editor = pref.edit();
                    editor.putString(regSrq.KEY_USER, "");
                    editor.putString(regSrq.KEY_ALAMAT, "");
                    editor.putString(regSrq.KEY_EMAIL, "");
                    editor.putString(regSrq.KEY_TELP, "");
                    editor.apply();

                }

                @Override
                public void onFailure(Call<reportSrq> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            });


        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }

        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
