package com.riodigital.puskesmasgambir.SubMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.riodigital.puskesmasgambir.ApiClient;
import com.riodigital.puskesmasgambir.Interface.RegisterAPI;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.model.ResponseDetail;
import com.riodigital.puskesmasgambir.model.ResponseMaster;
import com.riodigital.puskesmasgambir.model.Value;
import com.riodigital.puskesmasgambir.model.bindSrq;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SRQ extends AppCompatActivity {
    TextView noSrq,totalSrq, question;
    RadioGroup radioGroup;
    RadioButton ya, tidak;
    Button berikutnya;
    RegisterAPI registerAPI;
    List<bindSrq> listSrq = new ArrayList<>();
    ArrayList<bindSrq> simpanSrq;
    List<ResponseDetail> listDetail = new ArrayList<>();
    int lengthSrq, jawab, nomor = 0;
    private RegisterSRQ regSrq;
    private RegisterAPI regApi;
    private ProgressDialog progress;
    SharedPreferences pref;
    int maxId;
    Intent intent;
    String user, email, alamat, telp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srq);

        simpanSrq = new ArrayList<>();

        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Tunggu Sebentar...");
        progress.show();
        pref = getSharedPreferences(regSrq.KEY_PREF, Context.MODE_PRIVATE);

        noSrq = (TextView) findViewById(R.id.noSrq);
        totalSrq=(TextView)findViewById(R.id.totalSrq);
        question = (TextView) findViewById(R.id.question);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupSrq);
        ya = (RadioButton) findViewById(R.id.ya);
        tidak = (RadioButton) findViewById(R.id.tidak);
        berikutnya = (Button) findViewById(R.id.berikutnya);
        berikutnya.setEnabled(true);
        registerAPI = ApiClient.GetRetrofit().create(RegisterAPI.class);
        Call<Value> callBindSRQ = registerAPI.srqBinding();
        callBindSRQ.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                try {
                    listSrq = response.body().getResult();
                    lengthSrq = listSrq.size();
                    totalSrq.setText(" / "+lengthSrq);
                    updatePertanyaan();
                    progress.dismiss();
                }catch (Exception e){
                    Toast.makeText(SRQ.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                }

//                Toast.makeText(SRQ.this, "SRQ Berhasil di binding\n sizenya :"+listSrq.size(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(SRQ.this, "SRQ gagal Binding", Toast.LENGTH_SHORT).show();
            }
        });


        berikutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ya.isChecked() || tidak.isChecked()) {
                    nomor++;

                    jawab = ya.isChecked() ? 1 :
                            tidak.isChecked() ? 0 : 0;

                    if (nomor <= lengthSrq) { //batasnya setelah nomor kurang dari panjang baris pertanyaan

                        int nosrq;
                        String quest;
                        nosrq = Integer.valueOf(noSrq.getText().toString());
                        quest = question.getText().toString();
                        simpanSrq.add(new bindSrq(nosrq, quest, jawab));

                        if (nomor < lengthSrq) {
                            updatePertanyaan();
                        }else{
                            //Todo :  Jalankan Perintah tampil Laporan
                            berikutnya.setEnabled(false);


                            progress.setMessage("Tunggu Sebentar...");
                            progress.show();

                            simpanMasterDetail();
                        }
                        Log.d("nomor-lengthSrq:", String.valueOf(nomor)+"-"+String.valueOf(lengthSrq));
                    }
                }


            }
        });
    }

    public void simpanMasterDetail() {
        try{
            regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);
            Call<Value> callIdMax = regApi.max_id();
            callIdMax.enqueue(new Callback<Value>() {
                @Override
                public void onResponse(Call<Value> call, Response<Value> response) {

                    maxId = response.body().getMax_id() + 1;
                    user = pref.getString(regSrq.KEY_USER, "");
                    email = pref.getString(regSrq.KEY_EMAIL, "");
                    alamat = pref.getString(regSrq.KEY_ALAMAT, "");
                    telp = pref.getString(regSrq.KEY_TELP, "");

                    simpanSRQ();
                    progress.dismiss();
                    intent=new Intent(SRQ.this,HasilSRQ.class);
                    intent.putExtra("id_response",maxId);
                    startActivity(intent);
                    finish();

                }

                @Override
                public void onFailure(Call<Value> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),
                            t.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),
                    e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }

    }

    public void simpanSRQ() {
        regApi = null;
        regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);
        Call<Value> insertMaster = regApi.insertSrqMaster(maxId, alamat, telp, email, user);
        insertMaster.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();

                Toast.makeText(getApplicationContext(),
                        message.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });

        regApi = null;
        regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);

        for (bindSrq simpansrq : simpanSrq) {
            int getId,getjawab;
            getId=simpansrq.getId();
            getjawab=simpansrq.getJawab();
            Call<Value> insertDetail = regApi.insertSrqDetail(maxId,getId, getjawab, user);
            insertDetail.enqueue(new Callback<Value>() {
                @Override
                public void onResponse(Call<Value> call, Response<Value> response) {
                    String value = response.body().getValue();
                    String message = response.body().getMessage();
                           /* Toast.makeText(getApplicationContext(),
                                    message.toString(), Toast.LENGTH_SHORT).show();*/

                }

                @Override
                public void onFailure(Call<Value> call, Throwable t) {
                           /* Toast.makeText(getApplicationContext(),
                                    t.getMessage().toString(), Toast.LENGTH_SHORT).show();*/
                }
            });
        }


        Toast.makeText(getApplicationContext(), "Data kamu dan jawaban berhasil disimpan",
                Toast.LENGTH_SHORT).show();
    }

    public void updatePertanyaan() {

        noSrq.setText("" + listSrq.get(nomor).getId());
        question.setText(listSrq.get(nomor).getPertanyaan());
        radioGroup.check(0);


    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(SRQ.this);
        dialog.setTitle("Form SRQ");
        dialog.setCancelable(false);
        dialog.setMessage("Anda Yakin ingin batal?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        dialog.create().show();
    }
}
