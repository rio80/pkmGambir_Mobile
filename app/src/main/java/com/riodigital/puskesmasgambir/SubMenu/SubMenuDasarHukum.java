package com.riodigital.puskesmasgambir.SubMenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

public class SubMenuDasarHukum extends AppCompatActivity {
    TextView dasarHukum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu_dasar_hukum);
        dasarHukum=(TextView) findViewById(R.id.dasar_hukum);
        dasarHukum.setText(Html.fromHtml(getString(R.string.dasar_hukum)));
    }
}
