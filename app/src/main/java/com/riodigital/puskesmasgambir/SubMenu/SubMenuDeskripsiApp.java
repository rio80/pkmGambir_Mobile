package com.riodigital.puskesmasgambir.SubMenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.webkit.WebView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

public class SubMenuDeskripsiApp extends AppCompatActivity {
   WebView deskripsi;
    String URL="file:///android_asset/deskripsi_app.htm";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu_deskripsi_app);
       /* deskripsiApp=(TextView) findViewById(R.id.deskripsi_app);
        deskripsiApp.setText(Html.fromHtml(getString(R.string.deskripsi_app)));*/

        deskripsi=(WebView)findViewById(R.id.deskripsi);
        deskripsi.loadUrl(URL);
    }
}
