package com.riodigital.puskesmasgambir.SubMenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.riodigital.puskesmasgambir.R;

public class SubMenuFasKes extends AppCompatActivity {

    WebView webPeta;
    String URL="file:///android_asset/multiplemarker.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu_fas_kes);

        webPeta=(WebView)findViewById(R.id.petapuskesmas);
        WebSettings webSettings=webPeta.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webPeta.loadUrl(URL);
    }
}
