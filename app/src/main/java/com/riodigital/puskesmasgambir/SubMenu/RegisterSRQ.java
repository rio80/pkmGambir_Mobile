package com.riodigital.puskesmasgambir.SubMenu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.riodigital.puskesmasgambir.ApiClient;
import com.riodigital.puskesmasgambir.Interface.RegisterAPI;
import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.model.ResponseMaster;
import com.riodigital.puskesmasgambir.model.Value;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterSRQ extends AppCompatActivity {
    EditText editUser, editAlamat, editEmail, editTelp;
    Button btnRegister, btnBatal;
    SharedPreferences pref;
    public final static String KEY_PREF = "REGISTER_SRQ";
    public final static String KEY_IDRESPONSE = "ID_RESPONSE";
    public final static String KEY_USER = "USER_RESPONSE";
    public final static String KEY_EMAIL = "EMAIL_RESPONSE";
    public final static String KEY_ALAMAT = "ALAMAT_RESPONSE";
    public final static String KEY_TELP = "TELP_RESPONSE";
    RegisterAPI regApi;
    String email, user,alamat,telp;
    int ID_MAX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_srq);

        pref = getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);


        editUser = (EditText) findViewById(R.id.user);
        editEmail = (EditText) findViewById(R.id.email);
        editAlamat = (EditText) findViewById(R.id.alamat);
        editTelp = (EditText) findViewById(R.id.telp);
        btnRegister = (Button) findViewById(R.id.registerNext);
        btnBatal = (Button) findViewById(R.id.registerBatal);
        btnRegister.setEnabled(true);
        user = pref.getString(KEY_USER, "");
        if (!TextUtils.isEmpty(user)) {
            editUser.setText(pref.getString(KEY_USER, ""));
            editEmail.setText(pref.getString(KEY_EMAIL, ""));
            editAlamat.setText(pref.getString(KEY_ALAMAT, ""));
            editTelp.setText(pref.getString(KEY_TELP, ""));
            Toast.makeText(this,
                    "Hasil SRQ anda Belum Tersimpan di Database kami",
                    Toast.LENGTH_SHORT).show();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = editEmail.getText().toString();
                user = editUser.getText().toString();
                alamat = editAlamat.getText().toString();
                telp = editTelp.getText().toString();

                if (TextUtils.isEmpty(user)) {
                    editUser.setError("Mohon isikan Nama Lengkap Anda!");
                } else if (TextUtils.isEmpty(email)) {
                    editEmail.setError("Mohon isikan Email Anda!");
                } else if (TextUtils.isEmpty(alamat)) {
                    editAlamat.setError("Mohon isikan Alamat Lengkap Anda!");
                } else if (TextUtils.isEmpty(telp)) {
                    editTelp.setError("Mohon isikan No telp/HP Anda!");
                } else {
                    try {
                        btnRegister.setEnabled(false);
                        regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);
                        Call<Value> callCekUser = regApi.cekUser(email, user);
                        callCekUser.enqueue(new Callback<Value>() {
                            @Override
                            public void onResponse(Call<Value> call, Response<Value> response) {
                                String value = response.body().getValue();
                                String message = response.body().getMessage();

                                //Todo : Cek Email Di tabel, Jika Email sudah ada maka Value = 1
                                if (value == "1") {
                                    //todo : Kemudian Cek di tanggal apakah selisih Tanggal di Database
                                    //todo : dengan Tanggal di Server lebih dari 3 hari
                                    cekTanggal();
                                } else {
                                    //todo : Jika Belum ada Email yg terDaftar maka lanjut isi Form SRQ
                                    lanjutKeFormSRQ();
                                }
                            }

                            @Override
                            public void onFailure(Call<Value> call, Throwable t) {
                                Toast.makeText(RegisterSRQ.this,
                                        t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }catch (Exception e){
                        Toast.makeText(RegisterSRQ.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }



                }
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public long Notif(String tglDb, String tglSrv, String pattern) {
        long tampil = 0;
        try {
            SimpleDateFormat date = new SimpleDateFormat(pattern);
            Date TglDb = (Date) date.parse(tglDb);
            Date TglSrv = (Date) date.parse(tglSrv);
            long selisih = Math.abs(TglSrv.getTime() - TglDb.getTime());
            tampil = TimeUnit.MILLISECONDS.toDays(selisih);
        } catch (Exception e) {
            Toast.makeText(this,
                    e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
        return tampil;
    }

    public void cekTanggal() {
        try {
            regApi = ApiClient.GetRetrofit().create(RegisterAPI.class);
            Call<Value> callCekTanggal = regApi.cekTanggal(email);

            callCekTanggal.enqueue(new Callback<Value>() {
                @Override
                public void onResponse(Call<Value> call, Response<Value> response) {
                    String value = response.body().getValue();
                    long selisihTgl = 0;
                    int tmpSelisihTgl=0;
                    if (value == "1") {
                        String tglDb = response.body().getTglDb().toString();
                        String tglSrv = response.body().getTglSrv().toString();
                        selisihTgl = Notif(tglDb, tglSrv, "yyyy-MM-dd");
                        tmpSelisihTgl=Integer.valueOf(String.valueOf(selisihTgl));
                    }
                    if (tmpSelisihTgl > 3) {
                        //todo : Jika Email ada dan selisih Tanggal DB dan Server lebih dari 3
                        //todo : Maka lanjut isi Form SRQ
                        lanjutKeFormSRQ();
                    } else {
                        AlertDialog.Builder pesan = new AlertDialog.Builder(RegisterSRQ.this);
                        pesan.setCancelable(false);
                        pesan.setMessage("Untuk bisa cek lagi\n " +
                                "tunggu " +String.valueOf(4-tmpSelisihTgl)  + " hari lagi ya!");
                        pesan.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                        pesan.create().show();
                    }
                }

                @Override
                public void onFailure(Call<Value> call, Throwable t) {
                    Toast.makeText(RegisterSRQ.this,
                            t.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(this,
                    "CekTanggal Error Di :\n" + e.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
        }

    }

    public void lanjutKeFormSRQ() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(RegisterSRQ.this);
        dialog.setCancelable(false);
        dialog.setTitle("Register SRQ");
        dialog.setMessage("Anda Yakin lanjut ke Form SRQ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Todo : Buat perintah simpan Register Master
                //Todo : Set Register ke sharefPreferences
                String user, email, alamat, telp;
                user = editUser.getText().toString();
                email = editEmail.getText().toString();
                alamat = editAlamat.getText().toString();
                telp = editTelp.getText().toString();

                final SharedPreferences.Editor editor = pref.edit();

                editor.putString(KEY_USER, user);
                editor.putString(KEY_ALAMAT, alamat);
                editor.putString(KEY_EMAIL, email);
                editor.putString(KEY_TELP, telp);
                editor.apply();

                Toast.makeText(RegisterSRQ.this,
                        pref.getString(KEY_USER, "") + "\n" +
                                pref.getString(KEY_ALAMAT, "") + "\n" +
                                pref.getString(KEY_EMAIL, "") + "\n" +
                                pref.getString(KEY_TELP, "") + "\n", Toast.LENGTH_SHORT).show();
                //Todo : Setelah simpan, lanjut ke Activity form SRQ
                startActivity(new Intent(RegisterSRQ.this, SRQ.class));
                finish();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnRegister.setEnabled(true);
            }
        });
        dialog.create().show();
    }
}
