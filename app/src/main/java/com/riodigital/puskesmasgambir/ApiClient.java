package com.riodigital.puskesmasgambir;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rio senjou on 21/12/2017.
 */

public class ApiClient {
//    private final static String URL="http://192.168.43.123:81/puskesmasGambir/transaksi/api/";
    private final static String URL="http://gkm.puskesmaskecgambir.web.id/transaksi/API/";
    private static Retrofit retrofit;
    public  static Retrofit GetRetrofit(){
        if (retrofit==null){
            retrofit=new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
