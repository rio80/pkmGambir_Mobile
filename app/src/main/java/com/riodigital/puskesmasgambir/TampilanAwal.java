package com.riodigital.puskesmasgambir;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 17/08/2017.
 */
//Berfungsi untuk menampilkan SubMenu atau content dari Tombol Menu Utama

public class TampilanAwal extends AppCompatActivity {
    Button btnPuskesmasGambir, btnInfoPelayanan, btnInfoKes, btnLaporan, btnDeteksiDini, btnExit;


    public void init() {
        btnPuskesmasGambir = (Button) findViewById(R.id.btnPuskesmasGambir);
        btnInfoPelayanan = (Button) findViewById(R.id.btnInfoPelayanan);
        btnInfoKes = (Button) findViewById(R.id.btnInfoKes);
        btnLaporan = (Button) findViewById(R.id.btnLaporan);
        btnDeteksiDini = (Button) findViewById(R.id.btnDeteksiDini);

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tes_layout);
        init();

        btnPuskesmasGambir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("indexMenu",0);
                startActivity(i);
            }
        });

        btnInfoPelayanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("indexMenu",1);
                startActivity(i);
            }
        });

        btnInfoKes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("indexMenu",2);
                startActivity(i);
            }
        });
        btnLaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("indexMenu",3);
                startActivity(i);
            }
        });
        btnDeteksiDini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("indexMenu",4);
                startActivity(i);
            }
        });
    }


}
