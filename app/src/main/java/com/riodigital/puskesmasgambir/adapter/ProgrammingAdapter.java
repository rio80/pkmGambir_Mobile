package com.riodigital.puskesmasgambir.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 20/08/2017.
 */

public class ProgrammingAdapter extends ArrayAdapter<String> {
    private Activity act;
    private String[] nama_program;
    private String[] keterangan;
    private Integer[] gambar;

    public ProgrammingAdapter(Activity act,
                              ViewGroup container,
                              String[] nama_program,
                              String[] keterangan,
                              Integer[] gambar) {
        super(act, R.layout.fragment1, nama_program);
        this.act = act;
        this.nama_program = nama_program;
        this.keterangan = keterangan;
        this.gambar = gambar;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = act.getLayoutInflater();
        View view1 = inflater.inflate(R.layout.item_programming, null, true);

        TextView text_program = view1.findViewById(R.id.text_nama);
        TextView text_ket = view1.findViewById(R.id.text_ket);
        ImageView image_program = view1.findViewById(R.id.image_programming);

        text_program.setText(nama_program[position]);
        text_ket.setText(keterangan[position]);
        image_program.setImageResource(gambar[position]);

        return view1;
    }
}
