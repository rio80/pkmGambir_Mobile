package com.riodigital.puskesmasgambir.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 25/08/2017.
 */

public class InfoPelGridAdapter extends ArrayAdapter<String> {
    private Activity activity;
    private String[] values;
    private int[] images;
    private View v;

    public InfoPelGridAdapter(
                              Activity activity,
                              String[] values,
                              int[] images) {
        super(activity,R.layout.activity_dash_board,values);
        this.activity = activity;
        this.values = values;
        this.images = images;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inf=activity.getLayoutInflater();
        v=inf.inflate(R.layout.single_item,null);

        TextView textView=v.findViewById(R.id.textview);
        ImageView imageView=v.findViewById(R.id.imageview);

        textView.setText(values[position]);
        imageView.setImageResource(images[position]);

        return v;
    }
}
