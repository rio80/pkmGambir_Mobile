package com.riodigital.puskesmasgambir.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;
import com.riodigital.puskesmasgambir.model.ItemSlideMenu;

import java.util.List;

/**
 * Created by rio senjou on 17/08/2017.
 */

public class SlidingMenuAdapter extends BaseAdapter {
    private Context context;
    private List<ItemSlideMenu> lstItem;

    public SlidingMenuAdapter(Context context, List<ItemSlideMenu> lstItem) {
        this.context = context;
        this.lstItem = lstItem;
    }

    @Override
    public int getCount() {
        return lstItem.size();
    }

    @Override
    public Object getItem(int i) {
        return lstItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v=View.inflate(context, R.layout.item_sliding_menu,null);
        ImageView img=v.findViewById(R.id.item_img);
        TextView text=v.findViewById(R.id.item_title);

        ItemSlideMenu item=lstItem.get(i);

        img.setImageResource(item.getImgId());
        text.setText(item.getTitle());

        return v;
    }


}
