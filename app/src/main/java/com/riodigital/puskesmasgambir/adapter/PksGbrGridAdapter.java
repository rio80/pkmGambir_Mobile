package com.riodigital.puskesmasgambir.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 25/08/2017.
 */

public class PksGbrGridAdapter extends BaseAdapter{
    private Activity activity;
    private String[] values;
    private int[] images;
    View v;


    public PksGbrGridAdapter(Activity activity, String[] values, int[] images) {
        this.activity = activity;
        this.values = values;
        this.images = images;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inf=activity.getLayoutInflater();
        v=inf.inflate(R.layout.single_item,null,true);

        TextView textView=v.findViewById(R.id.textview);
        ImageView imageView=v.findViewById(R.id.imageview);

        textView.setText(values[i]);
        imageView.setImageResource(images[i]);

        return v;
    }
}
