package com.riodigital.puskesmasgambir.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riodigital.puskesmasgambir.R;

/**
 * Created by rio senjou on 25/08/2017.
 */

public class DashboardGridAdapter extends BaseAdapter {
    Context context;
    private final String[] values;
    private final int [] images;
    View v;
    LayoutInflater layoutInflater;

    public DashboardGridAdapter(Context context, String[] values, int[] images) {
        this.context = context;
        this.values = values;
        this.images = images;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view==null){
            v=new View(context);
            v=layoutInflater.inflate(R.layout.single_item,null);
            ImageView imageView=v.findViewById(R.id.imageview);
            TextView textView=v.findViewById(R.id.textview);

            imageView.setImageResource(images[i]);
            textView.setText(values[i]);
        }
        return v;
    }
}
